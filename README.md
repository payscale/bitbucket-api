# Bitbucket API

An assortment of scripts utilizing Bitbucket's REST API for tasks such as detecting certain verbiage across all repos

API: https://developer.atlassian.com/bitbucket/api/2/reference/resource/

# Setup

Find your username (not your email):

- profile picture -> personal settings -> Username

Create a new App Password:

- profile picture -> personal settings -> App Passwords -> Create (with only read-only access to repos)

Create a `config.json` file in the root of the project with username and password:

```json
{
  "username": "user_name",
  "password": "123.."
}
```

Install dependencies

- `npm install`

# Scripts

- **scripts/main-branch.js**: find all repositories in the PayScale workspace that have 'master' as their main, default branch and create an Excel file with repo details.
    - To run: `node scripts/main-branch`; will produce an Excel file in the `reports` folder.

- **scripts/verbiage.js**: searches all code in the PayScale workspace to find usages of a given word
    - To run: `node scripts/verbiage <word>`; will produce an Excel file in the `reports` folder for given word.

- **scripts/libraries.js**: find usage of libraries in a repo
    - To run: `node scripts/libraries`; will need to modify the script to search for your repo and library; will produce an Excel file in the `reports` folder.

# Docs

- **docs/master-to-main.md**: basic documentation for renaming `master` branch to `main` in Git and
  Bitbucket, with checklists for changing hard-coded values in TeamCity and Octopus.

# Notes

git version >= 2.28.0 can specify default branch name on init:

  `git init --initial-branch=main`

Also configurable with the `init.defaultBranch` setting

(Git will probably ask for a default name during installation)
