# `master` to `main` Quick Reference

This document covers the basic steps for renaming a Git `master` branch to a more inclusively named
`main` branch. The basic steps for performing this operation in Git are presented here, though for
more information about the reasons for the steps, please see the references listed below.

There are also loose checklists for updating hard-coded references to the master branch in TeamCity
and Octopus. Every team's build-deploy configurations will be different, so these checklists are
not intended to be comprehensive. Rather, they are intended to provide a guide to the various
places that the master branch name could be hard-coded and cause issues in build-deploy pipelines.

## Renaming the `master` branch

### Steps

1. Move the `master` branch to `main`
1. Push `main` to remote repo
1. Point HEAD to `main` branch
1. Change default branch to `main` on Bitbucket (must be done before deleting master branch)
1. Delete `master` branch on the remote repo
1. Have teammates create new tracking connection to `origin/main` branch

### Renaming commands

```
# Step 1
# create main branch locally, taking the history from master
git branch -m master main

# Step 2
# push the new local main branch to the remote repo (GitHub)
git push -u origin main

# Step 3
# switch the current HEAD to the main branch
git symbolic-ref refs/remotes/origin/HEAD refs/remotes/origin/main

# Step 4
# change the default branch on Bitbucket to main
# from the repo home page, select Repository settings from the menu on the right
# select the Advanced menu at the bottom of the page
# set the Main branch drop-down to main

# Step 5
# delete the master branch on the remote
git push origin --delete master
```

### Teammate's commands

```
# Switch to the "master" branch:
$ git checkout master

# Rename it to "main":
$ git branch -m master main

# Get the latest commits (and branches!) from the remote:
$ git fetch

# Remove the existing tracking connection with "origin/master":
$ git branch --unset-upstream

# Create a new tracking connection with the new "origin/main" branch:
$ git branch -u origin/main
```

## TeamCity Checklist

Remember that TeamCity has `build template`s on which `build configuration`s are based. For the
list items below, it might be prudent to first check the build template for master branch
references, then check its dependent build configurations for possible overrides that still
reference the old master branch.

- Parameters
  - VcsBranchSpec and VcsDefaultBranch (used by TeamCity for build triggers, etc)
- Build Steps
- build scripts and arguments
- Triggers
  - VCS triggers

## Octopus Checklist

- Variables
- Process (steps and arguments/variable references)
- deploy scripts and arguments
- Channels

## References

1. Mortimer, S. (2020, July 23). 5 steps to change GitHub default branch from master to main.
   Retrieved December 03, 2020, from https://stevenmortimer.com/5-steps-to-change-github-default-branch-from-master-to-main/

1. How to rename the "master" branch to "main" in Git. (n.d.). Retrieved December 03, 2020, from
   https://www.git-tower.com/learn/git/faq/git-rename-master-to-main/
