const { createExcel, createExcelHeader, isPayScaleEmail } = require('./util');
const { formatDateLong, getMasterExcelName, getEmail, getSizeMB, getAuthor } = require('./util');
const { getReposWithMasterBranch } = require('./endpoints');
const { sendMail } = require('./email');

// are you ABSOLUTELY SURE you want to send potentially hundreds of emails?
// perhaps try a dry run where the first parameter of sendMail is your own payscale email address.
const SEND_EMAIL_TO_ALL = false;

async function detectMasterBranches() {
  console.log('Looking for PayScale repos with \'master\' as main branch\n...');
  const badRepos = await getReposWithMasterBranch();
  console.log(`Found ${badRepos.length} repos with \'master\' as main branch\n`);

  // Build data required by Excel library
  const specification = {
    repoName: createExcelHeader('Repo Name', 160),
    authorName: createExcelHeader('Author', 200),
    email: createExcelHeader('Email', 300),
    description: createExcelHeader('Description', 400),
    createdOn: createExcelHeader('Created On', 130),
    updatedOn: createExcelHeader('Updated On', 130),
    size: createExcelHeader('Size (MB)', 100)
  }

  console.log('Gathering data for report\n...');
  const excelData = badRepos.map(repo => {
    return {
      repoName: repo.name,
      authorName: getAuthor(repo.mainbranch.target.author),
      email: getEmail(repo.mainbranch.target.author.raw),
      description: repo.description,
      createdOn: formatDateLong(repo.created_on),
      updatedOn: formatDateLong(repo.updated_on),
      size: getSizeMB(repo.size)
    }
  });

  console.log('# of rows:', excelData.length);

  if (SEND_EMAIL_TO_ALL === true) {
    for (let i = 0; i < excelData.length; i++) {
      const { authorName, email, repoName } = excelData[i];
      const term = "master";
      const message = `Hey there ${authorName}! This is an automated email sent to ${email}, because you are listed as the owner of the repository ${repoName}. The #hackday-language group wrote a script to find repos that are using outdated and potentially harmful industry terms, such as master, slave, whitelist, and blacklist. In this case, the term "${term}" is present in the branch name of ${repoName}. When you get a chance, please take a look at the following confluence page for reasons why we thought this was worth doing a hackday project on, and-if you agree that it's worth doing-next steps for how to update the language: https://payscale.atlassian.net/wiki/spaces/PRD/pages/388137035/Words+Have+Power`;

      // requirement from IT: only send to internal email addresses.
      if (isPayScaleEmail(email)) {
        // let's not spam the mail server super hard because, quote
        // Office 365 users are limited by the following:
        //   10, 000 sent email messages per day.
        //   500 recipients total for a single email.
        //   30 emails sent per minute.
        // so 1 email every 2.5 seconds = 24 emails a minute, under the 30/min limit.
        setTimeout(() => {
          sendMail(email, `Please consider updating your bitbucket repo ${repoName}`, message);
        }, 2500 * i);
      }
    }
  }

  const excelName = getMasterExcelName();
  createExcel(excelName, specification, excelData, 'Repos');
  console.log(`Finished creating report ${excelName}\n`);
}

detectMasterBranches();
