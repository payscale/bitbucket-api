const { createExcel, createExcelHeader, isPayScaleEmail } = require('./util');
const { formatDateLong, getVerbiageExcelName, getEmail, getSizeMB, getAuthor } = require('./util');
const { getReposWithWord } = require('./endpoints');
const { sendMail } = require('./email');

// are you ABSOLUTELY SURE you want to send potentially hundreds of emails?
// perhaps try a dry run where the first parameter of sendMail is your own payscale email address.
const SEND_EMAIL_TO_ALL = false;

async function detectVerbiage(word) {
  console.log(`Looking for PayScale files/repos with '${word}'\n...`);
  let badRepos = await getReposWithWord(word);

  // filter out matches to node_modules files
  badRepos = badRepos.filter(repo => !repo.file.path.includes('node_modules'));

  console.log(`Found ${badRepos.length} files with '${word}'\n`);

  console.log('Gathering data for report\n...');

  // Build data required by Excel library
  const specification = {
    fileName: createExcelHeader('File', 450),
    repoName: createExcelHeader('Repo', 150),
    authorName: createExcelHeader('Author', 150),
    email: createExcelHeader('Email', 200),
    createdOn: createExcelHeader('Repo Created On', 130),
    updatedOn: createExcelHeader('Repo Updated On', 130),
    size: createExcelHeader('Repo Size (MB)', 100)
  }

  const excelData = badRepos.map(repo => {
    return {
      fileName: repo.file.path,
      repoName: repo.file.commit.repository.name,
      authorName: getAuthor(repo.file.commit.author),
      email: getEmail(repo.file.commit.author.raw),
      createdOn: formatDateLong(repo.file.commit.repository.created_on),
      updatedOn: formatDateLong(repo.file.commit.repository.updated_on),
      size: getSizeMB(repo.file.commit.repository.size)
    }
  });

  console.log('# of rows:', excelData.length);

  if (SEND_EMAIL_TO_ALL === true) {
    for (let i = 0; i < excelData.length; i++) {
      const { authorName, email, repoName, fileName } = excelData[i];
      const term = word;
      const message = `Hey there ${authorName}! This is an automated email sent to ${email}, because you are listed as the owner of the repository ${repoName}. The #hackday-language group wrote a script to find repos that are using outdated and potentially harmful industry terms, such as master, slave, whitelist, and blacklist. In this case, the term "${term}" is present in the content of ${repoName}, specifically in ${fileName}. When you get a chance, please take a look at the following confluence page for reasons why we thought this was worth doing a hackday project on, and-if you agree that it's worth doing-next steps for how to update the language: https://payscale.atlassian.net/wiki/spaces/PRD/pages/388137035/Words+Have+Power`;

      // requirement from IT: only send to internal email addresses.
      if (isPayScaleEmail(email) && word) {
        // let's not spam the mail server super hard because, quote
        // Office 365 users are limited by the following:
        //   10, 000 sent email messages per day.
        //   500 recipients total for a single email.
        //   30 emails sent per minute.
        // so 1 email every 2.5 seconds = 24 emails a minute, under the 30/min limit.
        setTimeout(() => {
          sendMail(email, `Please consider updating your bitbucket repo ${repoName}`, message);
        }, 2500 * i);
      }
    }
  }

  const excelName = getVerbiageExcelName(word);
  createExcel(excelName, specification, excelData, word);
  console.log(`Finished creating report ${excelName}\n`);
}

detectVerbiage(process.argv[2]);
