const moment = require('moment-mini');
const superagent = require('superagent');
const excel = require('node-excel-export');
const fs = require('fs');
const config = require('../config.json');

/*
  Util functions for bitbucket API
*/
async function get(url, query = undefined) {
  const { username, password } = config;
  const result = await superagent.get(url).auth(username, password).query(query);
  let allValues = [];

  if (result.body && result.body.values) {
    allValues = allValues.concat(result.body.values);

    // responses are paginated, so we need to get all results
    if (result.body.next) {
      const nextValues = await get(result.body.next);
      allValues = allValues.concat(nextValues);
    }
  }

  return allValues;
}
module.exports.get = get;

module.exports.getSingle = async function(url, query = undefined) {
  const { username, password } = config;
  const result = await superagent.get(url).auth(username, password).query(query);

  return result.body;
}

module.exports.buildFields = function(fields = []) {
  let s = '';
  fields.forEach((field, index) => {
    s += `values.${field}`;
    if (index !== fields.length - 1) {
      s += ',';
    }
  });

  return s + ',next'; // next returns the url for the next set of paginated results
}

module.exports.createExcel = function(excelName, specification, data, sheetName) {
  // Create the excel report.
  // This function will return Buffer
  const report = excel.buildExport(
    [ // <- Notice that this is an array. Pass multiple sheets to create multi sheet report
      {
        name: sheetName,
        specification: specification,
        data
      }
    ]
  );

  fs.writeFileSync(`./reports/${excelName}.xlsx`, report);
}

module.exports.createExcelHeader = function(name, width) {
  const styles = {
    headerDark: {
      fill: {
        fgColor: {
          rgb: 'FF000000'
        }
      },
      font: {
        color: {
          rgb: 'FFFFFFFF'
        },
        sz: 14,
        bold: true,
        underline: true
      }
    }
  };

  return {
    displayName: name,
    headerStyle: styles.headerDark,
    width
  }
}

module.exports.formatDateLong = function(value) {
  return moment(value).format('MM-DD-YYYY');
}

module.exports.getMasterExcelName = function() {
  const nowFormatted = moment(new Date()).format('MM-DD-YYYY');

  return `repos-with-master-branch-${nowFormatted}`;
}

module.exports.getVerbiageExcelName = function(word) {
  const nowFormatted = moment(new Date()).format('MM-DD-YYYY');

  return `files-with-${word}-${nowFormatted}`;
}

module.exports.getLibraryName = function(library) {
  return library.replace('@', '').replace('/', '-');
}

module.exports.getEmail = function(raw) {
  return raw.substring(raw.lastIndexOf('<') + 1, raw.lastIndexOf('>'));
}

module.exports.getAuthor = function(author) {
  if (author && author.user && author.user.display_name) {
    return author.user.display_name;
  }

  const raw = author.raw;

  return raw.substring(0, raw.lastIndexOf('<')).trim();
}

module.exports.getSizeMB = function(size) {
  return (size/1048576).toFixed(2);
}

module.exports.getLineInfo = function(line) {
  const info = [];
  const split = line.split("'");

  if (split[1]) {
    const importPath = split[1].replace(';', '').replace("'", '').trim();

    if (importPath) {
      let importSplit;
      let importSplit2;
      importSplit = line.split('}')[0];
      if (importSplit) {
        importSplit2 = importSplit.split('{')[1];
        if (!importSplit2) {
          importSplit2 = importSplit.split(' ')[1];
        }
        if (importSplit2) {
          const imports = importSplit2.split(',');
  
          for (let i = 0; i < imports.length; i++) {
            const importComponent = imports[i].trim();
            info.push({ importPath, importComponent });
          }
        }
      }
    }
  }

  return info;
}

module.exports.isPayScaleEmail = function (email) {
  if (!email) {
    return false;
  }
  const domain = email.split('@')[1];
  if (!domain) {
    return false;
  }
  return domain.trim().toLowerCase() === 'payscale.com';
}
