const { get, getSingle, buildFields } = require('./util');

const BASE_URL = 'https://api.bitbucket.org/2.0';

/**
 * Functions utilizing different bitbucket API endpoints
 * Note: pagelen max is 100
*/

/* Get all projects in PayScale workspace */
module.exports.getProjects = async function() {
  const url = `${BASE_URL}/workspaces/PayScale/projects`;

  return await get(url);
}

/* Get all repositories */
module.exports.getRepos = async function() {
  const url = `${BASE_URL}/repositories/PayScale`;
  const query = {
    pagelen: 100
  }

  return await get(url, query);
}

/* Get all repositories with 'master' as the main branch */
module.exports.getReposWithMasterBranch = async function() {
  const url = `${BASE_URL}/repositories/PayScale`;

  const fields = buildFields([
    'description',
    'size',
    'created_on',
    'updated_on',
    'uuid',
    'name',
    'mainbranch.target.author.raw',
    'mainbranch.target.author.user.display_name'
  ]);

  const query = {
    pagelen: 100,
    q: `mainbranch.name="master"`,
    fields
  }

  return await get(url, query);
}

/* Get all repositories and files with a given word */
module.exports.getReposWithWord = async function(word) {
  const url = `${BASE_URL}/workspaces/PayScale/search/code`;

  const fields = buildFields([
    'file.path',
    'file.commit.author.user.display_name',
    'file.commit.author.raw',
    'file.commit.repository.name',
    'file.commit.repository.updated_on',
    'file.commit.repository.created_on',
    'file.commit.repository.size'
  ]);

  const query = {
    pagelen: 50,
    search_query: word,
    fields
  }
  return await get(url, query);
}

/* Get all repositories using a given library */
module.exports.getReposWithLibrary = async function(repo, library) {
  const url = `${BASE_URL}/workspaces/PayScale/search/code`;

  const fields = buildFields([
    'file.path'
  ]);

  const query = {
    pagelen: 100,
    search_query: `repo:${repo} ${library}`,
    fields
  }

  return await get(url, query);
}

/* Get all branches in a given repo */
module.exports.getBranches = async function(repoId) {
  const url = `${BASE_URL}/repositories/PayScale/${repoId}/refs/branches`;
  const query = {
    pagelen: 100
  }

  return await get(url, query);
}

/* Get branch details with given repoId and branchName */
module.exports.getBranchDetails = async function(repoId, branchName) {
  const url = `${BASE_URL}/repositories/PayScale/${repoId}/refs/branches/${branchName}`;

  return await getSingle(url);
}