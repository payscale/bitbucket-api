const { createExcel, createExcelHeader } = require('./util');
const { getLineInfo, getVerbiageExcelName, getLibraryName } = require('./util');
const { getReposWithLibrary } = require('./endpoints');
const lineByLine = require('n-readlines');

async function findLibraryUsage(repo, library) {
  console.log(`Looking for PayScale files/repos with '${library}'\n...`);
  let results = await getReposWithLibrary(repo, library);

  // filter out matches to node_modules, package.json, package-lock.json, scss, css, or ts typings
  results = results.filter(file => {
    return !file.file.path.includes('node_modules') &&
      !file.file.path.includes('.css') &&
      !file.file.path.includes('.scss') &&
      !file.file.path.includes('package.json') &&
      !file.file.path.includes('package-lock.json') &&
      !file.file.path.includes('.d.ts') &&
      !file.file.path.includes('.d.tsx')
  });

  console.log(`Found ${results.length} files for '${library}'\n`);
  console.log('Gathering data for report\n...');

  const data = [];

  results.forEach((file) => {
    // this assumes you have the bitbucket-api next to the target repo in your filesystem
    // and you're running this from the root of bitbucket-api
    const filePath = file.file.path;
    const path = `../${repo}/${filePath}`;
    const liner = new lineByLine(path);
    let line;

    let package;
    if (filePath.includes('packages/ps-components')) {
      package = 'ps-components';
    } else if (filePath.includes('packages/marketpay')) {
      package = 'marketpay';
    } else if (filePath.includes('packages/panther-react')) {
      package = 'panther-react';
    }

    while (line = liner.next()) {
      const lineString = line.toString('utf-8');
      if (lineString.includes(library)) {
        const lineInfo = getLineInfo(lineString);
        for (let i = 0; i < lineInfo.length; i++) {
          const { importPath, importComponent } = lineInfo[i];

          if (!(package === 'ps-components' && importPath.includes('ps-components'))) {
            data.push({ package, filePath, importPath, importComponent });
          }
        }
      }
    }
  });

  // Build data required by Excel library
  const specification = {
    package: createExcelHeader('Package', 150),
    filePath: createExcelHeader('File Path', 700),
    importPath: createExcelHeader('Import Path', 500),
    importComponent: createExcelHeader('Import Component', 200),
  };

  const excelData = data;
  const libraryName = getLibraryName(library);
  const excelName = getVerbiageExcelName(libraryName);

  createExcel(excelName, specification, excelData, libraryName);
  console.log(`Finished creating report ${excelName}\n`);
}

findLibraryUsage('ps-ui', '@payscale/aspect-design-system');
findLibraryUsage('ps-ui', '@payscale/payscale-shared-ui');
findLibraryUsage('ps-ui', '@payscale/design');
findLibraryUsage('ps-ui', 'ps-components/src');
findLibraryUsage('ps-ui', 'react-bootstrap/lib');
